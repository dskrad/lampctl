import requests, json, datetime, unittest, sys, os

class MyTests(unittest.TestCase):
  def test_get_time(self):
    resp = type(get_time("sunrise"))
    self.assertEquals(resp.__name__, "int")
  def test_is_sun_phase(self):
    resp = is_sun_phase(0, 10, 5)
    self.assertTrue(resp)
    resp = is_sun_phase(0, 10, 15)
    self.assertFalse(resp)
  def test_retrieve_data(self):
    resp = retrieve_data()
    self.assertTrue(resp)
  def test_retrieve_data_is_dict(self):
    resp = retrieve_data()
    self.assertEquals(type(resp).__name__, "dict")

URL = "http://api.wunderground.com/api/ec260856a11d5e86/astronomy/q/CT/Hartford.json"
DATAFILE = os.environ["HOME"] + "/bin/lampctl/data.json"
NOW = datetime.datetime.now()

def retrieve_data():
  """Retrieves data from file, and if out of date then the
  server is queried and the response is saved to the datafile.
  Returns json response as a dict"""
  try: # try to open data file
    with open(DATAFILE, "r") as f:
      js0n = json.load(f)
    # parse time_stamp of data file
    time_stamp = datetime.strptime(js0n["time_stamp"], "%Y%m%dT%H%M")
    if NOW.day > time_stamp.day: # check if today > time_stamp day
      raise                      # if True, proceed to except statement
  except:
    js0n = json.loads(requests.get(URL).content)     # retrieve new data
    js0n["time_stamp"] = NOW.strftime("%Y%m%dT%H%M") # add time_stamp to json response
    with open(DATAFILE, "w") as f:
      json.dump(js0n, f, indent=2)                   # output js0n to data file
  return js0n

JS0N = retrieve_data()

def get_time(sun_verb):
  dct = JS0N.get("sun_phase").get(sun_verb)
  dct = {key: int(val) for key, val in dct.iteritems()}
  dt = datetime.datetime.combine(NOW, datetime.time(**dct))
  return int(dt.strftime("%s"))

def is_sun_phase(rise, sunset, now):
  if rise < now < sunset:
    return True
  else: False

sunrise = get_time("sunrise")
sunset = get_time("sunset")
now_ = int(datetime.datetime.now().strftime("%s"))

if is_sun_phase(sunrise, sunset, now_):
  diffh = (sunset-now_)/60.0/60
  diffm = int((diffh - int(diffh)) * 60)
  print "The sun is up now and will set\
 in {} hours {} minutes.".format(int(diffh), diffm)
else:
  print "The sun is set"

if __name__ == "__main__":
  unittest.main()
